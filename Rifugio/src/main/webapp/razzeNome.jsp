<%@page import="it.pegaso.rifugio.models.Razza"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Nome e razza</title>
</head>
<body>
	<% List<Razza> list = (List<Razza>)request.getAttribute("result"); %>

<form>
	<input type="text" name="nome">
<select id="specie" name= "specie">
	<% for(Razza r: list){ %>
		<option value="<%=r.getId() %>"><%=r.getDescrizione() %></option>
		
	<% } %>
</select>
<input type="submit">
</form>

</body>

</html>