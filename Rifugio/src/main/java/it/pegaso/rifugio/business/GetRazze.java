package it.pegaso.rifugio.business;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.pegaso.rifugio.dao.RazzeDao;
import it.pegaso.rifugio.models.Razza;

/**
 * Servlet implementation class GetRazze
 */
public class GetRazze extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetRazze() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idFromPage = request.getParameter("specie");
		Long id = Long.parseLong(idFromPage);
		RazzeDao dao = new RazzeDao();
		List<Razza> result = dao.getBySpecie(id);
		
		
		
		
		request.setAttribute("result", result);
		RequestDispatcher rd = request.getRequestDispatcher("razzeNome.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
