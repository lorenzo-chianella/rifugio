package it.pegaso.rifugio.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.pegaso.rifugio.models.Specie;

public class SpecieDao {
	
	private final static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/animali?user=root&password=root&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

	public List<Specie> getAll(){
		List<Specie> result = new ArrayList<Specie>();
		try {
			ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM specie");
			while(rs.next()) {
				Specie s = new Specie();
				s.setId(rs.getLong("id"));
				s.setDescrizione(rs.getString("descrizione"));
				result.add(s);	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	private Connection connection;

	public Connection getConnection() throws SQLException {
		if (connection == null) {
			connection = DriverManager.getConnection(CONNECTION_STRING);
		}
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
}
