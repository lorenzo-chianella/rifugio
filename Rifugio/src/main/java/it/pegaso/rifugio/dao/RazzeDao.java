package it.pegaso.rifugio.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.pegaso.rifugio.models.Razza;

public class RazzeDao {

	private final static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/animali?user=root&password=root&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private PreparedStatement razzaByIdSpecie;
	
	public List<Razza> getAll(){
		List<Razza> result = new ArrayList<Razza>();
		try {
			ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM razza");
			while(rs.next()) {
				Razza r = new Razza();
				r.setId(rs.getLong("id"));
				r.setDescrizione(rs.getString("descrizione"));
				result.add(r);	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public List<Razza> getBySpecie(Long id){
		List<Razza> result = new ArrayList<Razza>();
		try {
			getRazzaByIdSpecie().clearParameters();
			getRazzaByIdSpecie().setLong(1, id);
			ResultSet rs = getRazzaByIdSpecie().executeQuery();
			while(rs.next()) {
				Razza r = new Razza();
				r.setId(rs.getLong("id"));
				r.setDescrizione(rs.getString("descrizione"));
				result.add(r);	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	private Connection connection;

	public Connection getConnection() throws SQLException {
		if (connection == null) {
			connection = DriverManager.getConnection(CONNECTION_STRING);
		}
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public PreparedStatement getRazzaByIdSpecie() throws SQLException {
		if (razzaByIdSpecie == null) {
			razzaByIdSpecie = getConnection().prepareStatement("SELECT * FROM razza WHERE id = ?");
		}
		return razzaByIdSpecie;
	}

	public void setRazzaByIdSpecie(PreparedStatement razzaById) {
		this.razzaByIdSpecie = razzaById;
	}
	
	
}
